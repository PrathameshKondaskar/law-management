package com.example.lawmanagement.models;

import java.util.List;

public class LawyerProfileModel {

    String userId,name,email,mobile,qualification,experience,address,docId,imageURL;
    List<String> lawType;
    public static final String FIREBASE_COLLECTION_LAWYERPROFILE= "LawyerProfile";
    public LawyerProfileModel() {
    }



    public LawyerProfileModel(String userId, String name, String email, String mobile, String qualification, String experience, String address, String docId, String imageURL, List<String> lawType) {
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.qualification = qualification;
        this.experience = experience;
        this.address = address;
        this.docId = docId;
        this.imageURL = imageURL;
        this.lawType = lawType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<String> getLawType() {
        return lawType;
    }

    public void setLawType(List<String> lawType) {
        this.lawType = lawType;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
