package com.example.lawmanagement.models;

public class CaseModel {

    public static final String FIREBASE_COLLECTION_CASES = "Cases";

    String name,email,mobile,date,time,caseType,comment,userId, assignTo,lname,lmobile,status,docId;

    public CaseModel() {
    }

    public CaseModel(String name, String email, String mobile, String date, String time, String caseType, String comment, String userId, String assignTo,
                     String lname, String lmobile, String status, String docId) {
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.date = date;
        this.time = time;
        this.caseType = caseType;
        this.comment = comment;
        this.userId = userId;
        this.assignTo = assignTo;
        this.lname = lname;
        this.lmobile = lmobile;
        this.status = status;
        this.docId = docId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(String assignTo) {
        this.assignTo = assignTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getLmobile() {
        return lmobile;
    }

    public void setLmobile(String lmobile) {
        this.lmobile = lmobile;
    }
}
