package com.example.lawmanagement.models;

import java.util.ArrayList;
import java.util.List;

public class LawModel {

    String id,name,section,description,category;
    List<String> keywords;
    public static final String FIREBASE_COLLECTION_USERINFO = "Laws";

    public LawModel() {
    }

    public LawModel(String id, String name, String section, String description, String category, List<String> keywords) {
        this.id = id;
        this.name = name;
        this.section = section;
        this.description = description;
        this.category = category;
        this.keywords = keywords;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }
}
