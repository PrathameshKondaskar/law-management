package com.example.lawmanagement.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lawmanagement.R;
import com.example.lawmanagement.models.LawModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class AdminMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //VIEWS
    private EditText editTextLawName,editTextSectionNo,editTextDescription,editTextKeyWords;
    private Spinner spinnerCategory;
    private Button buttonAddLaw;
    private ProgressDialog progressDialog;

    //VARIABLES
    String lawName,sectionNo,description,keywords,lawId,category;
    ArrayList<String> categories;
    List<String> keywordsList;


    //Firebase
    FirebaseFirestore mFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        keywordsList = new ArrayList<String>();
        mFirestore = FirebaseFirestore.getInstance();
        editTextLawName = findViewById(R.id.editTextLawName);
        editTextDescription = findViewById(R.id.editTextDescription);
        editTextKeyWords = findViewById(R.id.editTextKeyWords);
        editTextSectionNo = findViewById(R.id.editTextSectionNo);
        spinnerCategory= findViewById(R.id.spinnerCategoryLaw);
        buttonAddLaw =findViewById(R.id.buttonAddLaw);
        progressDialog = new ProgressDialog(this);

        buttonAddLaw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addLaw();
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        categories = new ArrayList<>();
        categories.add("Constitutional law");
        categories.add("Criminal law");
        categories.add("Contract law");
        categories.add("Labour law");
        categories.add("Company law");
        categories.add("Tort law");
        categories.add("Property law");
        categories.add("Tax law");
        CustomAdapter customAdapter = new CustomAdapter(AdminMainActivity.this, android.R.layout.simple_list_item_1, categories);
        spinnerCategory.setAdapter(customAdapter);


    }

    public void addLaw()
    {

        lawId = randomNo();
         lawName = editTextLawName.getText().toString();
         sectionNo = editTextSectionNo.getText().toString();
         keywords = editTextKeyWords.getText().toString().replace(" ","");
         description = editTextDescription.getText().toString();
         category = spinnerCategory.getSelectedItem().toString();

         String [] keywordsArray = keywords.split(",");
         keywordsList = Arrays.asList(keywordsArray);

        boolean shouldCancelLaw = false;
        View focusView = null;



        if (lawName.equals("")) {
            shouldCancelLaw = true;
            focusView = editTextLawName;
            editTextLawName.setError("Law name is a required field");
        }
        if (sectionNo.equals("")) {
            shouldCancelLaw = true;
            focusView = editTextSectionNo;
            editTextSectionNo.setError("Section no. is a required field");
        }
        if (description.equals("")) {
            shouldCancelLaw = true;
            focusView = editTextDescription;
            editTextDescription.setError("Description is a required field");
        }
        if (keywords.equals("")) {
            shouldCancelLaw = true;
            focusView = editTextKeyWords;
            editTextKeyWords.setError("Keywords is a required field");
        }
        else if (shouldCancelLaw) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else
        {
            progressDialog.setMessage("Adding Law...");
            progressDialog.show();
            progressDialog.setCancelable(false);

            LawModel lawModel = new LawModel(lawId,lawName,sectionNo,description,category,keywordsList);

            mFirestore.collection(LawModel.FIREBASE_COLLECTION_USERINFO)
                    .add(lawModel)
                    .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentReference> task) {
                            if(task.isSuccessful())
                            {
                                progressDialog.dismiss();
                                Toast.makeText(AdminMainActivity.this, "Law added", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(AdminMainActivity.this,AdminMainActivity.class));
                                finish();
                            }
                            else
                            {
                                progressDialog.dismiss();
                                Toast.makeText(AdminMainActivity.this, "Law not added", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });



        }



    }

    public class CustomAdapter extends ArrayAdapter<String> implements SpinnerAdapter {
        Context context;
        ArrayList<String> categories = new ArrayList<>();

        public CustomAdapter(@NonNull Context context, int resource, @NonNull ArrayList<String> objects) {
            super(context, resource, objects);
            this.context = context;
            this.categories = objects;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


            View view = super.getView(position, convertView, parent);

            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setTextColor(getResources().getColor(R.color.black));
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            textView.setGravity(Gravity.CENTER);
            Typeface typeface = ResourcesCompat.getFont(context, R.font.source_sans_pro_semibold);
            textView.setTypeface(typeface);
            return view;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            convertView = LayoutInflater.from(AdminMainActivity.this).inflate(
                    R.layout.custom_spinner, parent, false);
            TextView textView = (TextView) convertView.findViewById(R.id.text);
            textView.setText(categories.get(position));

            return convertView;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.admin_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_his) {
            startActivity(new Intent(this,AdminHistoryCasesActivity.class));

        }
        else if (id == R.id.nav_logout) {
            startActivity(new Intent(this,LoginActivity.class));
            finish();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public String randomNo() {
        char[] chars1 = "ABCDEF012GHIJKL345MNOPQR678STUVWXYZ9".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 6; i++) {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        return sb1.toString();
    }
}
