package com.example.lawmanagement.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.lawmanagement.R;
import com.example.lawmanagement.models.CaseModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CaseActivity extends AppCompatActivity {

    public static final String EMAIL = "email";
    public static final String L_NAME = "name";
    public static final String L_MOBILE = "mobile";
    //VIEW
    private EditText editTextName,editTextEmail,editTextMobile,editTextComments;
    private TextView tvDate,tvTime;
    private Spinner spinnerType;
    private Button buttonBookNow;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog mTimePicker;
    private ProgressDialog progressDialog;

    //VARIABLES
    String name,email,mobile,comments,date,time,caseType,userId,assignTo,lName,lMobile;
    ArrayList<String> categories;

    //FIREBASE
    FirebaseAuth mAuth;
    FirebaseUser mUser;
    FirebaseFirestore mFireStore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_case);

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mFireStore = FirebaseFirestore.getInstance();
        userId = mUser.getUid();

        editTextName = findViewById(R.id.editTextName);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextMobile = findViewById(R.id.editTextMobile);
        editTextComments = findViewById(R.id.editTextComments);
        tvDate = findViewById(R.id.tvDate);
        tvTime = findViewById(R.id.tvTime);
        spinnerType = findViewById(R.id.spinnerType);
        buttonBookNow = findViewById(R.id.buttonBookNow);
        progressDialog = new ProgressDialog(this);

        Intent intent = getIntent();
        assignTo = intent.getStringExtra(EMAIL);
        lName = intent.getStringExtra(L_NAME);
        lMobile = intent.getStringExtra(L_MOBILE);

        categories = new ArrayList<>();
        categories.add("Constitutional case");
        categories.add("Criminal case");
        categories.add("Contract case");
        categories.add("Labour case");
        categories.add("Company case");
        categories.add("Tort case");
        categories.add("Property case");
        categories.add("Tax case");
        CustomAdapterType customAdapter = new CustomAdapterType(CaseActivity.this, android.R.layout.simple_list_item_1,categories);
        spinnerType.setAdapter(customAdapter);


        Date currentdate = new Date();  // to get the date
        SimpleDateFormat df = new SimpleDateFormat("dd/M/yyyy"); // getting date in this format
        String formattedDate = df.format(currentdate.getTime());
        tvDate.setText(formattedDate);

        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm a");
        String currentTime1=dateFormat.format(currentTime);
        tvTime.setText(currentTime1);

        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                datePickerDialog = new DatePickerDialog(CaseActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                tvDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                mTimePicker = new TimePickerDialog(CaseActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        String AM_PM ;
                        if(selectedHour < 12) {
                            AM_PM = "AM";
                        } else {
                            AM_PM = "PM";
                        }



                        if (selectedHour < 10) {
                            tvTime.setText("0" + selectedHour + ":" + selectedMinute + " " + AM_PM);
                        }
                        if (selectedMinute < 10) {
                            tvTime.setText(selectedHour + ":0" + selectedMinute + " " + AM_PM);
                        }
                        if (selectedHour < 10 && selectedMinute < 10) {
                            tvTime.setText("0" + selectedHour + ":0" + selectedMinute + " " + AM_PM);
                        }
                        if (selectedHour > 10 && selectedMinute > 10){
                            tvTime.setText(selectedHour + ":" + selectedMinute + " " + AM_PM);
                        }

                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        buttonBookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                attemptToFileCase();
            }
        });

    }

    private void attemptToFileCase() {
        name = editTextName.getText().toString();
        email = editTextEmail.getText().toString();
        mobile = editTextMobile.getText().toString();
        caseType = spinnerType.getSelectedItem().toString();
        comments = editTextComments.getText().toString();
        time = tvTime.getText().toString();
        date = tvDate.getText().toString();

        boolean shouldCancelSignUp = false;
        View focusView = null;


        if (name.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextName;
            editTextName.setError("Name is a required field");
        }
        if (email.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextEmail;
            editTextEmail.setError("email is a required field");
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            shouldCancelSignUp = true;
            focusView = editTextEmail;
            editTextEmail.setError("email is not valid");
        }

        if (mobile.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. is a required field");
        }
        if (!mobile.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. is a invalid");

        }
        if (mobile.length() != 10 || mobile.matches(".*[a-z].*")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. must be of 10 digit");
        }
        if (date.equals("")) {
            shouldCancelSignUp = true;
            focusView = tvDate;
            tvDate.setError("Date is a required field");
        }
        if (time.equals("")) {
            shouldCancelSignUp = true;
            focusView = tvTime;
            tvTime.setError("Time is a required field");
        }
        if (comments.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextComments;
            editTextComments.setError("Comments is a required field");
        }


        if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else
        {
            progressDialog.setMessage("Assigning case...");
            progressDialog.show();
            progressDialog.setCancelable(false);

            CaseModel caseModel = new CaseModel(name,email,mobile,date,time,caseType,comments,userId,assignTo,lName,lMobile,"0","docId");


            mFireStore.collection(CaseModel.FIREBASE_COLLECTION_CASES)
                    .add(caseModel)
                    .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentReference> task) {
                            if (task.isSuccessful()) {
                                progressDialog.dismiss();
                                Toast.makeText(CaseActivity.this, "Case assign to lawyer", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(CaseActivity.this,CustomerHistoryCasesActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();

                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(CaseActivity.this, "Not Updated", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

        }


    }



    public class CustomAdapterType extends ArrayAdapter<String> implements SpinnerAdapter {
        Context context;
        ArrayList<String> categories = new ArrayList<>();

        public CustomAdapterType(@NonNull Context context, int resource, @NonNull ArrayList<String> objects) {
            super(context, resource, objects);
            this.context = context;
            this.categories = objects;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


            View view = super.getView(position, convertView, parent);

            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setTextColor(getResources().getColor(R.color. black));
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            textView.setGravity(Gravity.CENTER);
            Typeface typeface = ResourcesCompat.getFont(context, R.font.source_sans_pro_semibold);
            textView.setTypeface(typeface);
            return view;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            convertView = LayoutInflater.from(CaseActivity.this).inflate(
                    R.layout.custom_spinner, parent, false);
            TextView textView = (TextView) convertView.findViewById(R.id.text);
            textView.setText(categories.get(position));

            return convertView;
        }
    }
}
