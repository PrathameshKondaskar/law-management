package com.example.lawmanagement.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.lawmanagement.R;
import com.example.lawmanagement.UserSharedPreference;
import com.example.lawmanagement.models.LawyerProfileModel;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

public class LawyerDetailsActivity extends AppCompatActivity {

    //VIEW
    private TextView tvLawyerType;
    private EditText et_name, et_mobile, et_email, et_address, et_qualification, et_experience;
    private ImageView imageViewProfile;
    private Button buttonBook;
    UserSharedPreference userSharedPreference;

    public static final String EMAIL = "email";
    public static final String DETAILS = "details";

    String email,details;
    List<String> typeList;

    FirebaseFirestore mFirestore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lawyer_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mFirestore = FirebaseFirestore.getInstance();

        et_name = findViewById(R.id.et_name);
        et_address = findViewById(R.id.et_address);
        et_email = findViewById(R.id.et_email);
        et_mobile = findViewById(R.id.et_mobile);
        et_experience = findViewById(R.id.et_experience);
        et_qualification = findViewById(R.id.et_qualification);
        tvLawyerType = findViewById(R.id.tvLawyerType);
        imageViewProfile = findViewById(R.id.imageViewProfile);
        buttonBook = findViewById(R.id.buttonBook);


        buttonBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LawyerDetailsActivity.this,CaseActivity.class);
                intent.putExtra(CaseActivity.EMAIL,email);
                intent.putExtra(CaseActivity.L_NAME,et_name.getText().toString());
                intent.putExtra(CaseActivity.L_MOBILE,et_mobile.getText().toString());
                startActivity(intent);
            }
        });

        Intent intent = getIntent();

        email = intent.getStringExtra(EMAIL);
        Log.d("Email1",email);
        details = intent.getStringExtra(DETAILS);

        mFirestore.collection(LawyerProfileModel.FIREBASE_COLLECTION_LAWYERPROFILE)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            if (document.getData().get("email").toString().matches(email)) {

                                LawyerProfileModel lawyerProfileModel = document.toObject(LawyerProfileModel.class);

                                //mImageUri = Uri.parse(lawyerProfileModel.getImageURL());

                                et_name.setText(lawyerProfileModel.getName());
                                et_email.setText(lawyerProfileModel.getEmail());
                                et_address.setText(lawyerProfileModel.getAddress());
                                et_mobile.setText(lawyerProfileModel.getMobile());
                                et_qualification.setText(lawyerProfileModel.getQualification());
                                et_experience.setText(lawyerProfileModel.getExperience());
                                String item = "";
                                typeList = new ArrayList<>();
                                for (int i = 0; i < lawyerProfileModel.getLawType().size(); i++) {
                                    typeList.add(lawyerProfileModel.getLawType().get(i));
                                    item = item + lawyerProfileModel.getLawType().get(i);
                                    if ((i != lawyerProfileModel.getLawType().size() - 1)) {

                                        item = item + "\n► ";
                                    }
                                }
                                tvLawyerType.setText("► " + item);

                                Glide.with(getApplicationContext())
                                        .load(lawyerProfileModel.getImageURL())
                                        .into(imageViewProfile);
                            }


                        }


                    }
                });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
