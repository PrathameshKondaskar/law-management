package com.example.lawmanagement.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.lawmanagement.R;
import com.example.lawmanagement.UserSharedPreference;
import com.example.lawmanagement.models.LawModel;
import com.example.lawmanagement.models.LawyerProfileModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class ShowLawyersActivity extends AppCompatActivity {

    //VIEW
    Spinner spinnerType;
    Button buttonLawyerSearch;
    ListView listViewLawyers;
    private SwipeRefreshLayout pullRefreshLayout;

    //Firebase
    private FirebaseFirestore mFireStore;

    ArrayList<LawyerProfileModel> lawyerProfileModels;
    UserSharedPreference userSharedPreference;
    ArrayList<String> categories;
    ArrayList<String> emailList;
    String LawyerEmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_lawyers);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mFireStore = FirebaseFirestore.getInstance();
        pullRefreshLayout = findViewById(R.id.pullRefreshLayout);
        spinnerType = findViewById(R.id.spinnerType);
        buttonLawyerSearch = findViewById(R.id.buttonLawyerSearch);
        listViewLawyers = findViewById(R.id.listViewLawyer);
        lawyerProfileModels = new ArrayList<>();
        categories = new ArrayList<>();
        emailList = new ArrayList<>();

        buttonLawyerSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLawyersByType();
            }
        });
        pullRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        getLawyersFromFirebase();

                        pullRefreshLayout.setRefreshing(false);
                    }
                }, 3000);
            }

        });

          spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        categories = new ArrayList<>();
        categories.add("Constitutional lawyer");
        categories.add("Criminal lawyer");
        categories.add("Contract lawyer");
        categories.add("Labour lawyer");
        categories.add("Company lawyer");
        categories.add("Tort lawyer");
        categories.add("Property lawyer");
        categories.add("Tax lawyer");

        CustomAdapterType customAdapter = new CustomAdapterType(ShowLawyersActivity.this, android.R.layout.simple_list_item_1,categories);
        spinnerType.setAdapter(customAdapter);

        getLawyersFromFirebase();


    }

    public void getLawyersFromFirebase() {
        mFireStore.collection(LawyerProfileModel.FIREBASE_COLLECTION_LAWYERPROFILE)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {

                            lawyerProfileModels = (ArrayList<LawyerProfileModel>) task.getResult().toObjects(LawyerProfileModel.class);

                            CustomAdapter customAdapter = new CustomAdapter(ShowLawyersActivity.this, android.R.layout.simple_list_item_1, lawyerProfileModels);
                            listViewLawyers.setAdapter(customAdapter);


                        }

                    }
                });

    }


    public void getLawyersByType()
    {

        final String type = spinnerType.getSelectedItem().toString();


            mFireStore.collection(LawyerProfileModel.FIREBASE_COLLECTION_LAWYERPROFILE)
                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                            lawyerProfileModels = new ArrayList<>();
                            for (QueryDocumentSnapshot document : queryDocumentSnapshots) {

                                List<String> types = (List<String>) document.getData().get("lawType");
                                for(int i =0;i<types.size();i++) {
                                    if(types.get(i).matches(type)) {
                                        LawyerProfileModel lawyerProfileModel = document.toObject(LawyerProfileModel.class);
                                        lawyerProfileModels.add(lawyerProfileModel);
                                    }
                                }
                            }
                            CustomAdapter customAdapter = new CustomAdapter(ShowLawyersActivity.this, android.R.layout.simple_list_item_1, lawyerProfileModels);
                            listViewLawyers.setAdapter(customAdapter);

                        }
                    });


    }

    public class CustomAdapter extends ArrayAdapter<LawyerProfileModel> {
        Context context;
        ArrayList<LawyerProfileModel> lawyerProfileModels;

        public CustomAdapter(Context context, int resource, ArrayList<LawyerProfileModel> lawyerProfileModels) {
            super(context, resource, lawyerProfileModels);
            this.context = context;
            this.lawyerProfileModels = lawyerProfileModels;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            convertView = LayoutInflater.from(ShowLawyersActivity.this).inflate(
                    R.layout.custom_lawyer, parent, false);

            TextView tvLawyName = convertView.findViewById(R.id.tvLawyerName);
            TextView tvExperience = convertView.findViewById(R.id.tvExperience);
            TextView tvEmail = convertView.findViewById(R.id.tvEmail);
            TextView tvType = convertView.findViewById(R.id.tvType);
            ImageView imageViewProfile = convertView.findViewById(R.id.imageViewProfile);
            Button buttonCall = convertView.findViewById(R.id.buttonCall);
            Button buttonDetails = convertView.findViewById(R.id.buttonDetails);
            final String phone = lawyerProfileModels.get(position).getMobile();

           LawyerEmail = lawyerProfileModels.get(position).getEmail();
           emailList.add(LawyerEmail);

            tvLawyName.setText(lawyerProfileModels.get(position).getName());
            tvEmail.setText(lawyerProfileModels.get(position).getEmail());
            tvExperience.setText(lawyerProfileModels.get(position).getExperience() + "Years Experience");

            Glide.with(convertView)
                    .load(lawyerProfileModels.get(position).getImageURL())
                    .into(imageViewProfile);

            String type = "";
            for (int i = 0; i < lawyerProfileModels.get(position).getLawType().size(); i++) {
                type = type + lawyerProfileModels.get(position).getLawType().get(i);

                if (i != lawyerProfileModels.get(position).getLawType().size() - 1) {
                    type = type + ",";
                }
            }
            tvType.setText(type);

            buttonCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" +phone));
                    startActivity(callIntent);
                }
            });

            buttonDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(ShowLawyersActivity.this,LawyerDetailsActivity.class);
                    intent.putExtra(LawyerDetailsActivity.EMAIL,emailList.get(position));
                    Log.d("Email",emailList.get(position));
                    intent.putExtra(LawyerDetailsActivity.DETAILS,"1");
                    startActivity(intent);
                }
            });

            return convertView;

        }
    }

    public class CustomAdapterType extends ArrayAdapter<String> implements SpinnerAdapter {
        Context context;
        ArrayList<String> categories = new ArrayList<>();

        public CustomAdapterType(@NonNull Context context, int resource, @NonNull ArrayList<String> objects) {
            super(context, resource, objects);
            this.context = context;
            this.categories = objects;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


            View view = super.getView(position, convertView, parent);

            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setTextColor(getResources().getColor(R.color. black));
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            textView.setGravity(Gravity.CENTER);
            Typeface typeface = ResourcesCompat.getFont(context, R.font.source_sans_pro_semibold);
            textView.setTypeface(typeface);
            return view;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            convertView = LayoutInflater.from(ShowLawyersActivity.this).inflate(
                    R.layout.custom_spinner, parent, false);
            TextView textView = (TextView) convertView.findViewById(R.id.text);
            textView.setText(categories.get(position));

            return convertView;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
