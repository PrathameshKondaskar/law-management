package com.example.lawmanagement.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.lawmanagement.R;

import java.util.List;

public class MumbaiCourtActivity extends AppCompatActivity {

    ListView listView;
    ProgressDialog progressDialog;
    LocationManager locationManager,mLocationManager;
    String destinationLatitude,destinationLongitude;
    String[] courtsLocation = {"Azad Nagar, Sewri",
            " Chhatrapati Shivaji Terminus Area, Fort",
            "Mantralaya, Fort","St Xavier St,Parel",
            "Mahatma Gandhi Hospital, Parel",
            "Dadar East,Dadar",
            "Government Colony, Bandra",
            "Lohar Chawl, Kalbadevi",
            "Kala Ghoda, Churchgate",
            "Bandra Kurla Complex, Bandra",
            "Kurla West, Kurla",
            "Court Ln, Andheri",
            "Sundar Nagar, Borivali",
            "Jail Rd, Umerkhadi",
            "Chhatrapati Shivaji Terminus Area, Fort"};
    ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mumbai_court);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView)findViewById(R.id.list_view);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading....");
        progressDialog.setTitle("Fetching Path to Court");

        arrayAdapter = new ArrayAdapter<>(MumbaiCourtActivity.this,android.R.layout.simple_list_item_1,courtsLocation);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        destinationLatitude="18.996839";
                        destinationLongitude="72.852895";
                        break;
                    case 1:
                        destinationLatitude="18.941724";
                        destinationLongitude="72.832888";
                        break;
                    case 2:
                        destinationLatitude="18.927411";
                        destinationLongitude="72.830363";
                        break;
                    case 3:
                        destinationLatitude="19.006487";
                        destinationLongitude="72.844742";
                        break;
                    case 4:
                        destinationLatitude="18.998241";
                        destinationLongitude="72.841814";
                        break;
                    case 5:
                        destinationLatitude="19.006913";
                        destinationLongitude="72.843581";
                        break;
                    case 6:
                        destinationLatitude="19.061293";
                        destinationLongitude="72.850067";
                        break;
                    case 7:
                        destinationLatitude="18.944910";
                        destinationLongitude="72.830054";
                        break;
                    case 8:
                        destinationLatitude="18.932919";
                        destinationLongitude="72.832880";
                        break;
                    case 9:
                        destinationLatitude="19.055604";
                        destinationLongitude="72.851357";
                        break;
                    case 10:
                        destinationLatitude="19.063471";
                        destinationLongitude="72.873314";
                        break;
                    case 11:
                        destinationLatitude="19.119117";
                        destinationLongitude="72.849356";
                        break;
                    case 12:
                        destinationLatitude="19.229489";
                        destinationLongitude="72.854883";
                        break;
                    case 13:
                        destinationLatitude="18.959947";
                        destinationLongitude="72.836589";
                        break;
                    case 14:
                        destinationLatitude="18.941614";
                        destinationLongitude="72.832967";
                        break;

                }

                progressDialog.show();
                Location myLocation = getLastKnownLocation();
//                double longitude = myLocation.getLongitude();
                //              double latitude = myLocation.getLatitude();
                ///Toast.makeText(Main2Activity.this, ""+longitude+" "+latitude, Toast.LENGTH_SHORT).show();
                String uri = "http://maps.google.com/maps?f=d&hl=en&saddr=My+Location&daddr=" + destinationLatitude + "," + destinationLongitude;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(Intent.createChooser(intent, "Select an application"));
                progressDialog.cancel();
                // Toast.makeText(Main2Activity.this, ""+i, Toast.LENGTH_SHORT).show();

//                getLocation();

            }
        });

    }
    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        System.out.println("dfsdf");
        for (String provider : providers) {
            try {
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    // Found best last known location: %s", l);
                    bestLocation = l;
                }
            }catch (SecurityException e){
                Toast.makeText(this, ""+e, Toast.LENGTH_SHORT).show();
                System.out.println("abc"+e.toString());
            }

        }
        return bestLocation;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
