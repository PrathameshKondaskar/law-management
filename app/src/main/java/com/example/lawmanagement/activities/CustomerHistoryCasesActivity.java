package com.example.lawmanagement.activities;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.lawmanagement.R;
import com.example.lawmanagement.UserSharedPreference;
import com.example.lawmanagement.models.CaseModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import javax.annotation.Nullable;

public class CustomerHistoryCasesActivity extends AppCompatActivity {

    //View
    private ListView listViewHistory;

    //Variables
    ArrayList<CaseModel> caseModels;
    FirebaseFirestore mFireStore;
    UserSharedPreference userSharedPreference ;
    FirebaseAuth mAuth;
    FirebaseUser mUser;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_history_cases);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        userId = mUser.getUid();

        mFireStore = FirebaseFirestore.getInstance();
        caseModels = new ArrayList<>();
        listViewHistory = findViewById(R.id.listViewHistory);
        userSharedPreference = new UserSharedPreference(this);
        getHistoryOfCases();
    }


    public void getHistoryOfCases()
    {
        mFireStore.collection(CaseModel.FIREBASE_COLLECTION_CASES)
                .whereEqualTo("userId",userId)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

                        caseModels = new ArrayList<>();
                        for(QueryDocumentSnapshot documentSnapshot: queryDocumentSnapshots)
                        {

                                CaseModel caseModel = documentSnapshot.toObject(CaseModel.class);
                                caseModel.setDocId(documentSnapshot.getId());
                                caseModels.add(caseModel);


                        }
                        if(caseModels.size()>0){
                        CustomAdapter customAdapter = new CustomAdapter(CustomerHistoryCasesActivity.this,android.R.layout.simple_list_item_1,caseModels);
                        listViewHistory.setAdapter(customAdapter);}
                    }
                });


    }

    public class CustomAdapter extends ArrayAdapter<CaseModel> {

        Context context;
        ArrayList<CaseModel> caseModels;

        public CustomAdapter(Context context, int resource, ArrayList<CaseModel> caseModels) {
            super(context, resource, caseModels);
            this.context = context;
            this.caseModels = caseModels;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            view = LayoutInflater.from(CustomerHistoryCasesActivity.this).inflate(
                    R.layout.custom_history_cases, parent, false);

            TextView tvName = view.findViewById(R.id.tvName);
            TextView tvEmail = view.findViewById(R.id.tvEmail);
            TextView tvMobile = view.findViewById(R.id.tvMobile);
            TextView tvCaseType = view.findViewById(R.id.tvcaseType);
            TextView tvStatus = view.findViewById(R.id.tvStatus);
            TextView tvDate = view.findViewById(R.id.tvDate);



            if(caseModels.size()>0) {


                tvName.setText(caseModels.get(position).getLname());
                tvEmail.setText(caseModels.get(position).getAssignTo());
                tvMobile.setText(caseModels.get(position).getLmobile());
                tvCaseType.setText(caseModels.get(position).getCaseType());
                tvDate.setText(caseModels.get(position).getDate());
                if (caseModels.get(position).getStatus().matches("0")) {
                    tvStatus.setText("Not accepted yet...!");

                } else if (caseModels.get(position).getStatus().matches("1")) {
                    tvStatus.setText("ACCEPTED");

                } else {
                    tvStatus.setText("REJECTED");

                }
            }

            return view;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
