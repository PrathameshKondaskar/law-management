package com.example.lawmanagement.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.example.lawmanagement.R;
import com.example.lawmanagement.UserSharedPreference;
import com.example.lawmanagement.models.LawModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //VIEWS
    private EditText et_searchSection, et_searchKeyword;
    private Button buttonSectionSearch, buttonKeywordSearch;
    private ListView listViewLaw;
    private SwipeRefreshLayout pullRefreshLayout;
    private TextView tvNoLaw;
    //FIREBASE
    private FirebaseFirestore mFireStore;

    //VARIABLES
    ArrayList<LawModel> lawModelList;
    UserSharedPreference userSharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        userSharedPreference = new UserSharedPreference(this);
        pullRefreshLayout = findViewById(R.id.pullRefreshLayout);
        mFireStore = FirebaseFirestore.getInstance();
        et_searchSection = findViewById(R.id.et_searchSection);
        et_searchKeyword = findViewById(R.id.et_searchKeyWord);
        buttonKeywordSearch = findViewById(R.id.buttonKeywordSearch);
        buttonSectionSearch = findViewById(R.id.buttonSectionSearch);
        listViewLaw = findViewById(R.id.listViewLaw);
        tvNoLaw = findViewById(R.id.tvNoLaw);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        getLawsFromFirebase();
        buttonSectionSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLawsBySection();
            }
        });

        buttonKeywordSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLawsByKeyword();
            }
        });

        pullRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {


            @Override
            public void onRefresh() {
                pullRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        tvNoLaw.setVisibility(View.GONE);
                        getLawsFromFirebase();

                        pullRefreshLayout.setRefreshing(false);
                    }
                }, 3000);
            }

        });

        pullRefreshLayout.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
    }

    public void getLawsBySection()
    {


        final String section = et_searchSection.getText().toString();

        View focusView = null;
        if(section.matches(""))
        {
            focusView = et_searchSection;
            et_searchSection.setError("Please enter Section no.");
            focusView.requestFocus();

        }else {
            mFireStore.collection(LawModel.FIREBASE_COLLECTION_USERINFO)
                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                            lawModelList = new ArrayList<>();
                            for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                                if (document.getData().get("section").toString().matches(section)) {
                                    LawModel lawModel = document.toObject(LawModel.class);
                                    lawModelList.add(lawModel);
                                }
                            }
                            if(lawModelList.size()<1){
                                tvNoLaw.setVisibility(View.VISIBLE);
                                lawModelList = new ArrayList<>();
                                CustomAdapter customAdapter = new CustomAdapter(MainActivity.this, android.R.layout.simple_list_item_1, lawModelList);
                                listViewLaw.setAdapter(customAdapter);

                            }else {
                                tvNoLaw.setVisibility(View.GONE);
                                CustomAdapter customAdapter = new CustomAdapter(MainActivity.this, android.R.layout.simple_list_item_1, lawModelList);
                                listViewLaw.setAdapter(customAdapter);
                            }
                        }
                    });
        }

    }

    public void getLawsByKeyword()
    {

        final String keyword = et_searchKeyword.getText().toString();

        View focusView = null;
        if(keyword.matches(""))
        {
            focusView = et_searchKeyword;
            et_searchKeyword.setError("Please enter Keyword ");
            focusView.requestFocus();

        }else {
            mFireStore.collection(LawModel.FIREBASE_COLLECTION_USERINFO)
                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                            lawModelList = new ArrayList<>();
                            for (QueryDocumentSnapshot document : queryDocumentSnapshots) {

                                List<String> key = (List<String>) document.getData().get("keywords");
                                for(int i =0;i<key.size();i++) {
                                    if(key.get(i).matches(keyword)) {
                                        LawModel lawModel = document.toObject(LawModel.class);
                                        lawModelList.add(lawModel);
                                    }
                                }
                            }

                            if(lawModelList.size()<1){
                                tvNoLaw.setVisibility(View.VISIBLE);
                                lawModelList = new ArrayList<>();
                                CustomAdapter customAdapter = new CustomAdapter(MainActivity.this, android.R.layout.simple_list_item_1, lawModelList);
                                listViewLaw.setAdapter(customAdapter);

                            }else {
                                tvNoLaw.setVisibility(View.GONE);
                                CustomAdapter customAdapter = new CustomAdapter(MainActivity.this, android.R.layout.simple_list_item_1, lawModelList);
                                listViewLaw.setAdapter(customAdapter);
                            }

                        }
                    });
        }

    }
    public void getLawsFromFirebase() {
        mFireStore.collection(LawModel.FIREBASE_COLLECTION_USERINFO)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if(task.isSuccessful())
                        {

                             lawModelList = (ArrayList<LawModel>) task.getResult().toObjects(LawModel.class);

                            CustomAdapter customAdapter = new CustomAdapter(MainActivity.this, android.R.layout.simple_list_item_1, lawModelList);
                            listViewLaw.setAdapter(customAdapter);


                        }

                    }
                });

    }

    public class CustomAdapter extends ArrayAdapter<LawModel> {
        Context context;
        ArrayList<LawModel> lawModelList;

        public CustomAdapter(Context context, int resource, ArrayList<LawModel> lawModelList) {
            super(context, resource, lawModelList);
            this.context = context;
            this.lawModelList = lawModelList;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView = LayoutInflater.from(MainActivity.this).inflate(
                    R.layout.custom_law_list, parent, false);

            TextView tvSection = convertView.findViewById(R.id.tvSection);
            TextView tvLawname = convertView.findViewById(R.id.tvLawname);
            TextView tvDesc = convertView.findViewById(R.id.tvDesc);

            tvSection.setText("Section " + lawModelList.get(position).getSection());
            tvLawname.setText(lawModelList.get(position).getName());
            tvDesc.setText(lawModelList.get(position).getDescription());

            return convertView;

        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_lawyers) {
            startActivity(new Intent(this,ShowLawyersActivity.class));

        } else if (id == R.id.nav_history) {
            startActivity(new Intent(this,CustomerHistoryCasesActivity.class));

        }

        else if (id == R.id.nav_near) {
            startActivity(new Intent(this,NearByCourtsActivity.class));

        }
        else if (id == R.id.nav_mumbai) {
            startActivity(new Intent(this,MumbaiCourtActivity.class));

        }
        else if (id == R.id.nav_logout) {
            FirebaseAuth.getInstance().signOut();
            userSharedPreference.clearSharedPreferences();
            startActivity(new Intent(MainActivity.this,LoginActivity.class));
            finish();


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
