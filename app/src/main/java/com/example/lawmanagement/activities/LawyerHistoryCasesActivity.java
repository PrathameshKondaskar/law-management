package com.example.lawmanagement.activities;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.lawmanagement.R;
import com.example.lawmanagement.UserSharedPreference;
import com.example.lawmanagement.models.CaseModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

public class LawyerHistoryCasesActivity extends AppCompatActivity {

    //View
    private ListView listViewHistory;
    private TextView tvNoHistory;

    //Variables
    ArrayList<CaseModel> caseModels;
    FirebaseFirestore mFireStore;
    UserSharedPreference userSharedPreference ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lawyer_history_cases);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mFireStore = FirebaseFirestore.getInstance();
        caseModels = new ArrayList<>();
        listViewHistory = findViewById(R.id.listViewHistory);
        tvNoHistory = findViewById(R.id.tvNoHistory);
        userSharedPreference = new UserSharedPreference(this);

        getHistoryOfCases();
    }


    public void getHistoryOfCases()
    {
        mFireStore.collection(CaseModel.FIREBASE_COLLECTION_CASES)
                .whereEqualTo("assignTo",userSharedPreference.getEmail())
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        caseModels = new ArrayList<>();
                        for(QueryDocumentSnapshot documentSnapshot: queryDocumentSnapshots)
                        {
                            if(documentSnapshot.getData().get("status").toString().matches("1") || documentSnapshot.getData().get("status").toString().matches("2"))
                            {
                                CaseModel caseModel = documentSnapshot.toObject(CaseModel.class);
                                caseModel.setDocId(documentSnapshot.getId());
                                caseModels.add(caseModel);


                            }

                        }
                        if(caseModels.size()<1){
                            tvNoHistory.setVisibility(View.VISIBLE);
                            caseModels = new ArrayList<>();
                            CustomAdapter customAdapter = new CustomAdapter(LawyerHistoryCasesActivity.this, android.R.layout.simple_list_item_1, caseModels);
                            listViewHistory.setAdapter(customAdapter);

                        }else {

                            tvNoHistory.setVisibility(View.GONE);
                            CustomAdapter customAdapter = new CustomAdapter(LawyerHistoryCasesActivity.this, android.R.layout.simple_list_item_1, caseModels);
                            listViewHistory.setAdapter(customAdapter);
                        }
                    }
                });


    }


    public class CustomAdapter extends ArrayAdapter<CaseModel> {

        Context context;
        ArrayList<CaseModel> caseModels;

        public CustomAdapter(Context context, int resource, ArrayList<CaseModel> caseModels) {
            super(context, resource, caseModels);
            this.context = context;
            this.caseModels = caseModels;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            view = LayoutInflater.from(LawyerHistoryCasesActivity.this).inflate(
                    R.layout.custom_history_cases, parent, false);

            TextView tvName = view.findViewById(R.id.tvName);
            TextView tvEmail = view.findViewById(R.id.tvEmail);
            TextView tvMobile = view.findViewById(R.id.tvMobile);
            TextView tvCaseType = view.findViewById(R.id.tvcaseType);
            TextView tvStatus = view.findViewById(R.id.tvStatus);
            TextView tvDate = view.findViewById(R.id.tvDate);



            tvDate.setText(caseModels.get(position).getDate());
            tvName.setText(caseModels.get(position).getName());
            tvEmail.setText(caseModels.get(position).getEmail());
            tvMobile.setText(caseModels.get(position).getMobile());
            tvCaseType.setText(caseModels.get(position).getCaseType());
            if(caseModels.get(position).getStatus().matches("1"))
            {
                tvStatus.setText("ACCEPTED");

            }
            else
            {
                tvStatus.setText("REJECTED");

            }


            return view;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
