package com.example.lawmanagement.activities;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.lawmanagement.R;
import com.example.lawmanagement.UserSharedPreference;
import com.example.lawmanagement.models.LawyerProfileModel;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

public class LawyerMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String ADDRESS = "address";
    public static final String MOBILE = "mobile";
    public static final String FLAG = "flag";
    public static final String DETAILS = "details" ;


    //VIEWS
    private Button buttonSelect, buttonUpdate;
    private TextView tvLawyerType;
    private EditText et_name, et_mobile, et_email, et_address, et_qualification, et_experience;
    private ImageView imageViewProfile;
    private ProgressDialog progressDialog;
    UserSharedPreference userSharedPreference;

    //Variables
    String[] listItems;
    String[] selectedItems;
    boolean[] checkedItems;
    ArrayList<Integer> mUserItems = new ArrayList<>();
    String name, email, address, mobile, qualification, experience, flag, userId, docId,details;
    List<String> typeList;

    public static final int KITKAT_VALUE = 1002;

    Bitmap bm;
    int flags = 0;
    String imgurl;
    private static final int PICK_IMAGE_REQUEST = 1;
    private Uri mImageUri,nImageUri;
    Intent intents;


    //Firebase
    FirebaseFirestore mFirestore;
    private StorageReference mStorageRef;
    FirebaseAuth mAuth;
    FirebaseUser mUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lawyer_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mFirestore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        userId = mUser.getUid();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        intents =getIntent();

        typeList = new ArrayList<String>();
        userSharedPreference = new UserSharedPreference(this);
        et_name = findViewById(R.id.et_name);
        et_address = findViewById(R.id.et_address);
        et_email = findViewById(R.id.et_email);
        et_mobile = findViewById(R.id.et_mobile);
        et_experience = findViewById(R.id.et_experience);
        et_qualification = findViewById(R.id.et_qualification);
        buttonUpdate = findViewById(R.id.buttonUpdate);
        imageViewProfile = findViewById(R.id.imageViewProfile);
        progressDialog = new ProgressDialog(this);




      // mImageUri = Uri.parse(Uri.decode("content://com.android.providers.media.documents/document/image%3A438385"));

        imageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toUpdate();
            }
        });


        Intent intent = getIntent();

        flag = intent.getStringExtra(FLAG);
        name = intent.getStringExtra(NAME);
        email = intent.getStringExtra(EMAIL);
        address = intent.getStringExtra(ADDRESS);
        mobile = intent.getStringExtra(MOBILE);
        details = intent.getStringExtra(DETAILS);

        if(details!=null)
        {
            mFirestore.collection(LawyerProfileModel.FIREBASE_COLLECTION_LAWYERPROFILE)
                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

                            for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                                if (document.getData().get("email").toString().matches(email)) {
                                    docId = document.getId();
                                    LawyerProfileModel lawyerProfileModel = document.toObject(LawyerProfileModel.class);

                                    //mImageUri = Uri.parse(lawyerProfileModel.getImageURL());

                                    et_name.setText(lawyerProfileModel.getName());
                                    et_email.setText(lawyerProfileModel.getEmail());
                                    et_address.setText(lawyerProfileModel.getAddress());
                                    et_mobile.setText(lawyerProfileModel.getMobile());
                                    et_qualification.setText(lawyerProfileModel.getQualification());
                                    et_experience.setText(lawyerProfileModel.getExperience());
                                    String item = "";
                                    typeList = new ArrayList<>();
                                    for (int i = 0; i < lawyerProfileModel.getLawType().size(); i++) {
                                        typeList.add(lawyerProfileModel.getLawType().get(i));
                                        item = item + lawyerProfileModel.getLawType().get(i);
                                        if ((i != lawyerProfileModel.getLawType().size() - 1)) {

                                            item = item + "\n► ";
                                        }
                                    }
                                    tvLawyerType.setText("► " + item);

                                    Glide.with(getApplicationContext())
                                            .load(lawyerProfileModel.getImageURL())
                                            .into(imageViewProfile);
                                }


                            }


                        }
                    });



        }

        if (flag != null) {
            et_name.setText(name);
            et_mobile.setText(mobile);
            et_email.setText(email);
            et_address.setText(address);

        } else {
            getLawyersFromFirebase();


        }


        buttonSelect = findViewById(R.id.buttonSelect);
        tvLawyerType = findViewById(R.id.tvLawyerType);
        listItems = getResources().getStringArray(R.array.law_type);
        checkedItems = new boolean[listItems.length];

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        buttonSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder mBuilder = new AlertDialog.Builder(LawyerMainActivity.this);
                mBuilder.setTitle("select lawyer type");
                mBuilder.setMultiChoiceItems(listItems, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position, boolean isChecked) {

                        if (isChecked) {
                            if (!mUserItems.contains(position)) {
                                mUserItems.add(position);
                            }

                        } else if (mUserItems.contains(position)) {
                            mUserItems.remove(position);
                        }


                    }
                });

                mBuilder.setCancelable(false);
                mBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String item = "";
                        typeList= new ArrayList<>();
                        for (int i = 0; i < mUserItems.size(); i++) {

                            item = item + listItems[mUserItems.get(i)];
                            if (i != mUserItems.size() - 1) {
                                item = item + "\n► ";
                            }
                            typeList.add(listItems[i]);
                        }
                        //typeList = Arrays.asList();

                        tvLawyerType.setText("► " + item);
                    }
                });

                mBuilder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                mBuilder.setNeutralButton("Clear all", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        for (int i = 0; i < checkedItems.length; i++) {
                            checkedItems[i] = false;
                            mUserItems.clear();
                            tvLawyerType.setText("");

                        }

                    }
                });

                AlertDialog mDialog = mBuilder.create();
                mDialog.show();

            }
        });
    }

    public void toUpdate() {
        final String uName = et_name.getText().toString();
        final String uEmail = et_email.getText().toString();
        final String uMobile = et_mobile.getText().toString();
        final String uAddress = et_address.getText().toString();
        final String uQualification = et_qualification.getText().toString();
        final String uExperience = et_experience.getText().toString();


        boolean shouldCancelLaw = false;
        View focusView = null;


        if (uName.equals("")) {
            shouldCancelLaw = true;
            focusView = et_name;
            et_name.setError("Name is a required field");
        }
        if (uEmail.equals("")) {
            shouldCancelLaw = true;
            focusView = et_email;
            et_email.setError("Email is a required field");
        }
        if (uMobile.equals("")) {
            shouldCancelLaw = true;
            focusView = et_mobile;
            et_mobile.setError("Mobile is a required field");
        }
        if (uAddress.equals("")) {
            shouldCancelLaw = true;
            focusView = et_address;
            et_address.setError("Address is a required field");
        }
        if (uQualification.equals("")) {
            shouldCancelLaw = true;
            focusView = et_qualification;
            et_qualification.setError("Qualification is a required field");
        }
        if (uExperience.equals("")) {
            shouldCancelLaw = true;
            focusView = et_experience;
            et_experience.setError("Experience is a required field");
        }
        if (flags == 0) {
            Toast.makeText(this, "Image Not Selected", Toast.LENGTH_SHORT).show();
        }
        if (typeList.size() <= 0) {
            Toast.makeText(this, "Please select the Lawyer Type", Toast.LENGTH_SHORT).show();
        } else if (shouldCancelLaw) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            progressDialog.setMessage("Updating Profile...");
            progressDialog.show();
            progressDialog.setCancelable(false);

            if (mImageUri != null) {


                final String path = System.currentTimeMillis() + "." + getExtenssion(mImageUri);
                final StorageReference fileRef = mStorageRef.child(path);


                fileRef.putFile(mImageUri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return fileRef.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            Uri downUri = task.getResult();
                            Log.d("PATH", "onComplete: Url: " + downUri.toString());

                            LawyerProfileModel lawyerProfileModel = new LawyerProfileModel(userId, uName, uEmail, uMobile, uQualification, uExperience
                                    , uAddress, "docId", downUri.toString(), typeList);


                            if (docId != null) {
                                Map<String, Object> map = new HashMap<>();
                                map.put("name", uName);
                                map.put("email", uEmail);
                                map.put("mobile", uMobile);
                                map.put("address", uAddress);
                                map.put("qualification", uQualification);
                                map.put("experience", uExperience);
                                map.put("lawType", typeList);
                                map.put("imageURL", downUri.toString());


                                mFirestore.collection(LawyerProfileModel.FIREBASE_COLLECTION_LAWYERPROFILE)
                                        .document(docId)
                                        .update(map)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                progressDialog.dismiss();
                                                Toast.makeText(LawyerMainActivity.this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                            } else {
                                mFirestore.collection(LawyerProfileModel.FIREBASE_COLLECTION_LAWYERPROFILE).add(lawyerProfileModel)
                                        .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentReference> task) {
                                                if (task.isSuccessful()) {
                                                    progressDialog.dismiss();
                                                    Toast.makeText(LawyerMainActivity.this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();

                                                } else {
                                                    progressDialog.dismiss();
                                                    Toast.makeText(LawyerMainActivity.this, "Not Updated", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                            }
                        }
                    }
                });

            }
            else
            {
                if (docId != null) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("name", uName);
                    map.put("email", uEmail);
                    map.put("mobile", uMobile);
                    map.put("address", uAddress);
                    map.put("qualification", uQualification);
                    map.put("experience", uExperience);
                    map.put("lawType", typeList);



                    mFirestore.collection(LawyerProfileModel.FIREBASE_COLLECTION_LAWYERPROFILE)
                            .document(docId)
                            .update(map)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    progressDialog.dismiss();
                                    Toast.makeText(LawyerMainActivity.this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                                }
                            });
                }

            }
        }

    }


    public void getLawyersFromFirebase() {

        mFirestore.collection(LawyerProfileModel.FIREBASE_COLLECTION_LAWYERPROFILE)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            if (document.getData().get("userId").toString().matches(userId)) {
                                docId = document.getId();
                                LawyerProfileModel lawyerProfileModel = document.toObject(LawyerProfileModel.class);

                                //mImageUri = Uri.parse(lawyerProfileModel.getImageURL());

                                et_name.setText(lawyerProfileModel.getName());
                                et_email.setText(lawyerProfileModel.getEmail());
                                et_address.setText(lawyerProfileModel.getAddress());
                                et_mobile.setText(lawyerProfileModel.getMobile());
                                et_qualification.setText(lawyerProfileModel.getQualification());
                                et_experience.setText(lawyerProfileModel.getExperience());
                                String item = "";
                                typeList = new ArrayList<>();
                                for (int i = 0; i < lawyerProfileModel.getLawType().size(); i++) {
                                    typeList.add(lawyerProfileModel.getLawType().get(i));
                                    item = item + lawyerProfileModel.getLawType().get(i);
                                    if ((i != lawyerProfileModel.getLawType().size() - 1)) {

                                        item = item + "\n► ";
                                    }
                                }
                                tvLawyerType.setText("► " + item);

                                Glide.with(getApplicationContext())
                                        .load(lawyerProfileModel.getImageURL())
                                        .into(imageViewProfile);
                            }


                        }


                    }
                });


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.lawyer_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

            startActivity(new Intent(this,LawyerPendingCasesActivity.class));

        } else if (id == R.id.nav_slideshow) {
            startActivity(new Intent(this,LawyerHistoryCasesActivity.class));
        }  else if (id == R.id.nav_send) {
            FirebaseAuth.getInstance().signOut();
            userSharedPreference.clearSharedPreferences();
            startActivity(new Intent(LawyerMainActivity.this, LoginActivity.class));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    //Image taking from Gallery
    private void openFileChooser() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            mImageUri = data.getData();
            userSharedPreference.setUri(mImageUri.toString());
            Log.d("path", mImageUri.toString());
            imageViewProfile.setImageURI(mImageUri);
            //Picasso.with(this).load(mImageUri).into(mImageView);
            //Bitmap bitmap = (Bitmap)data.getExtras().get(data.getData().toString());
            //mImageView.setImageBitmap(bitmap);
            imageViewProfile.buildDrawingCache();
            flags = 1;

        }

    }

    private String getExtenssion(Uri uri) {
        ContentResolver cr = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cr.getType(uri));

//        String result;
//        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
//        if (cursor == null) { // Source is Dropbox or other similar local file path
//            result = uri.getPath();
//        } else {
//            cursor.moveToFirst();
//            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
//            result = cursor.getString(idx);
//            cursor.close();
//        }
//        return result;

    }

}
