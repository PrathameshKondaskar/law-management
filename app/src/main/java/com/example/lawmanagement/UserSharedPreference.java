package com.example.lawmanagement;

import android.content.Context;
import android.content.SharedPreferences;

public class UserSharedPreference {


    private final String FILENAME = "LawManagement";
    private final String KEY_ROLE = "role";
    private final String KEY_ADDRESS = "address";
    private final String KEY_MOBILE = "mobile";
    private final String KEY_NAME = "name";
    private final String KEY_EMAIL = "email";
    private final String KEY_URI = "uri";


    // VARIABLES
    private SharedPreferences sharedPreferences;

    public UserSharedPreference(Context context) {
        sharedPreferences = context.getSharedPreferences(FILENAME, Context.MODE_PRIVATE);
    }

    private void putValue(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void setRole(String role) {
        putValue(KEY_ROLE, role);
    }

    public String getRole() {
        return sharedPreferences.getString(KEY_ROLE, null);
    }


    public void setAddress(String address) {
        putValue(KEY_ADDRESS, address);
    }

    public String getAddress() {
        return sharedPreferences.getString(KEY_ADDRESS, null);
    }

    public void setMobile(String mobile) {
        putValue(KEY_MOBILE, mobile);
    }

    public String getMobile() {
        return sharedPreferences.getString(KEY_MOBILE, null);
    }


    public void setName(String name) {
        putValue(KEY_NAME, name);
    }

    public String getName() {
        return sharedPreferences.getString(KEY_NAME, null);
    }


    public void setEmail(String email) {
        putValue(KEY_EMAIL, email);
    }

    public String getEmail() {
        return sharedPreferences.getString(KEY_EMAIL, null);
    }

    public void setUri(String uri) {
        putValue(KEY_URI, uri);
    }

    public String getUri() {
        return sharedPreferences.getString(KEY_URI, null);
    }




    public void clearSharedPreferences() {
        setAddress(null);
        setRole(null);
        setMobile(null);
        setEmail(null);
        setName(null);
    }
}
